import 'card/card_color.dart';
import 'card/card_type.dart';
import 'deck.dart';
import 'interfaces/game_interface.dart';
import 'player/player.dart';
import 'player/players.dart';

class Game<P extends Player> {
  static const int _cardsPerPlayer = 4;

  final Deck _deck;
  final Players<P> _players;
  final GameInterface<P> _gameInterface;

  CardColor? _colorOnChanger;
  int? _overChargeCount;

  int _round;
  bool _gameRunning;

  Game(this._players, this._gameInterface)
      : _deck = Deck(),
        _gameRunning = false,
        _round = 0;

  void prepareGame() {
    _deck.setupDeck();
    _players.clearPlayersDecks();

    for (int i = 0; i < _cardsPerPlayer; i++) {
      for (Player player in _players.allPlayers) {
        player.addCardToDeck(_deck.retrieveCard());
      }
    }
  }

  void startGame() {
    if(!_gameRunning) {
      _gameRunning = true;

      run();
    }
  }

  Future<void> run() async {
    _gameInterface.gameStart();
    var currentCard = _deck.retrieveCard();

    if(currentCard.type == CardType.menic) {
      _colorOnChanger = currentCard.color;
    } else if(currentCard.type == CardType.eso) {
      _overChargeCount = 0;
    } else if(currentCard.type == CardType.seven) {
      _overChargeCount = 1;
    }

    _gameInterface.showFirstCard(currentCard);

    while (_players.remainingPlayerCount > 1) {
      for (P player in _players.remainingPlayers) {
        _gameInterface.currentPlayer(player);

        if (currentCard.isSpecial && _overChargeCount != null) {
          final selectedCard = await _gameInterface.letPlayerSelectCard(player, currentCard, null, _overChargeCount);

          if (selectedCard != null) {
            if (selectedCard.type == CardType.seven) {
              _overChargeCount = _overChargeCount! + 1;
            }

            player.removeCardFromDeck(selectedCard);
            _deck.addPlacedCard(selectedCard);
            _gameInterface.placeCard(selectedCard);
            currentCard = selectedCard;

            if(!player.isPlaying) {
              _players.playerEnded(player);
            }
          } else {
            if (currentCard.type == CardType.seven) {
              final cardCount = 2 * _overChargeCount!;

              player.addCardsToDeck(_deck.retrieveCards(cardCount));
              _gameInterface.playerRetrievedOverChargedCards(player, cardCount);
            }

            _overChargeCount = null;
          }
        } else {
          var selectedCard = await _gameInterface.letPlayerSelectCard(player, currentCard, _colorOnChanger, _overChargeCount);

          if (selectedCard != null) {
            if (selectedCard.type == CardType.menic) {
              _colorOnChanger = await _gameInterface.letPlayerSelectColor();
            } else if (selectedCard.type == CardType.seven) {
              _overChargeCount = 1;
            } else if (selectedCard.type == CardType.eso) {
              _overChargeCount = 0;
            }

            player.removeCardFromDeck(selectedCard);
            _deck.addPlacedCard(selectedCard);
            _gameInterface.placeCard(selectedCard);
            currentCard = selectedCard;

            if(!player.isPlaying) {
              _players.playerEnded(player);
            }
          } else {
            var retrievedCard = _deck.retrieveCard();

            player.addCardToDeck(retrievedCard);
            _gameInterface.addCardToDeck(player, retrievedCard);
          }
        }

        if(_players.isEnd) {
          break;
        }
      }

      _round++;
    }

    _gameInterface.gameOver(_players, _round);
  }
}
