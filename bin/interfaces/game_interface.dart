import '../card/card.dart';
import '../card/card_color.dart';
import '../player/player.dart';
import '../player/players.dart';

abstract class GameInterface<P extends Player> {
  void gameStart();
  Future<Card?> letPlayerSelectCard(P player, Card currentCard, CardColor? colorOnChanger, int? overChargeCount);
  void showFirstCard(Card currentCard);
  void placeCard(Card selectedCard);
  void addCardToDeck(P player, Card retrievedCard);
  Future<CardColor?> letPlayerSelectColor();
  void playerRetrievedOverChargedCards(P player, int count);
  void gameOver(Players players, int round);
  void currentPlayer(P player);
}
