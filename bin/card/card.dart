import 'card_color.dart';
import 'card_type.dart';

class Card implements Comparable<Card> {
  final CardColor _color;
  final CardType _type;

  const Card._(this._color, this._type);

  CardColor get color => _color;
  CardType get type => _type;
  bool get isSpecial => _isCardTypeSpecial(type);
  bool get isSuperSpecial => _isCardTypeSuperSpecial(type);

  @override
  String toString() {
    return 'Card{color: $_color, type: $_type}';
  }

  @override
  int compareTo(Card other) {
      return (other._type == _type || other._color == _color) ? 0 : -1;
  }

  static bool _isCardTypeSuperSpecial(CardType type) {
    return type == CardType.menic;
  }

  static bool _isCardTypeSpecial(CardType type) {
    return CardType.seven == type || CardType.eso == type;
  }
}

class Cards {
  static final List<Card> _allCards = _createCards();

  static List<Card> _createCards() {
    return const [
      Card._(CardColor.cerveny, CardType.eight),
      Card._(CardColor.cerveny, CardType.eso),
      Card._(CardColor.cerveny, CardType.kral),
      Card._(CardColor.cerveny, CardType.menic),
      Card._(CardColor.cerveny, CardType.nine),
      Card._(CardColor.cerveny, CardType.seven),
      Card._(CardColor.cerveny, CardType.svrsek),
      Card._(CardColor.cerveny, CardType.x),

      Card._(CardColor.zaludy, CardType.eight),
      Card._(CardColor.zaludy, CardType.eso),
      Card._(CardColor.zaludy, CardType.kral),
      Card._(CardColor.zaludy, CardType.menic),
      Card._(CardColor.zaludy, CardType.nine),
      Card._(CardColor.zaludy, CardType.seven),
      Card._(CardColor.zaludy, CardType.svrsek),
      Card._(CardColor.zaludy, CardType.x),

      Card._(CardColor.kule, CardType.eight),
      Card._(CardColor.kule, CardType.eso),
      Card._(CardColor.kule, CardType.kral),
      Card._(CardColor.kule, CardType.menic),
      Card._(CardColor.kule, CardType.nine),
      Card._(CardColor.kule, CardType.seven),
      Card._(CardColor.kule, CardType.svrsek),
      Card._(CardColor.kule, CardType.x),

      Card._(CardColor.listy, CardType.eight),
      Card._(CardColor.listy, CardType.eso),
      Card._(CardColor.listy, CardType.kral),
      Card._(CardColor.listy, CardType.menic),
      Card._(CardColor.listy, CardType.nine),
      Card._(CardColor.listy, CardType.seven),
      Card._(CardColor.listy, CardType.svrsek),
      Card._(CardColor.listy, CardType.x),
    ];
  }

  static List<Card> get allCards => _allCards;
}
