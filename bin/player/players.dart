import 'dart:collection';

import 'player.dart';

class Players<P extends Player> {
  final Set<P> _players;
  final Set<P> _remainingPlayers;
  final Queue<P> _winner;

  Players._(this._players, this._remainingPlayers, this._winner);

  factory Players(Set<P> players) {
    final allPlayers = UnmodifiableSetView(players);
    final remainingPlayers = Set.of(players);

    if(players.length < 2) {
      throw Exception("Not Enough players to play this game!");
    }

    return Players._(allPlayers, remainingPlayers, Queue());
  }

  void clearPlayersDecks() {
    for (var p in _players) {
      p.clearDeck();
    }
  }

  Set<P> get remainingPlayers {
    return Set.unmodifiable(_remainingPlayers);
  }

  int get remainingPlayerCount {
    return _remainingPlayers.length;
  }

  void playerEnded(P player) {
    if(_remainingPlayers.contains(player)) {
      _remainingPlayers.remove(player);

      _winner.add(player);
    }
  }

  Queue<P> get winner => _winner;
  Set<P> get allPlayers => _players;
  bool get isEnd => remainingPlayerCount == 1;
}
