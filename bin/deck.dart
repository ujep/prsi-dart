import 'dart:collection';
import 'dart:core';

import 'card/card.dart';

class Deck {
  final List<Card> _cards;
  final List<Card> _remainingCards;
  final List<Card> _placedCards;

  Deck._(this._cards, this._remainingCards, this._placedCards);

  factory Deck() {
    final cards = Cards.allCards;

    return Deck._(
      UnmodifiableListView(cards),
      [],
      [],
    );
  }

  void setupDeck() {
    _placedCards.clear();

    _remainingCards.clear();
    _remainingCards.addAll(_cards);
    _remainingCards.shuffle();
  }

  Card retrieveCard() {
    if(_remainingCards.isNotEmpty) {
      return _remainingCards.removeAt(0);
    }

    _reroll();
    return retrieveCard();
  }

  List<Card> retrieveCards(int count) {
    return [
      for (int i = 0; i < count; i++)
          retrieveCard()
    ];
  }

  void _reroll() {
    _remainingCards.addAll(_placedCards.reversed);

    _placedCards.clear();
  }

  void addPlacedCard(Card card) {
    _placedCards.add(card);
  }
}
